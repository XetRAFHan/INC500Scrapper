# INC500 Spider (世界5000强公司爬虫)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)
[![Build Status](https://travis-ci.org/{ORG-or-USERNAME}/{REPO-NAME}.png?branch=master)](https://travis-ci.org/{ORG-or-USERNAME}/{REPO-NAME})

> Target(目标): https://www.inc.com/inc5000/list/2017

INC500 Spider Scrap all the company informations from the target then save it into a excel file, so that you can analysis the INC500 companies (actually 5000 are scrapped) company informations. 

## Scrapped Title:
* CompanyID
* Ranking
* CompanyName
* Industry
* Growth
* Revenue
* City
* StateAbbr
* StateName
* YearsOnINCList
* Partner
* BriefDescription
* Description
* Leadership
* Founded
* ThreeYearGrowth
* Employees
* Website
* Location
* WikipediaPage
* WikipediaURL

## Getting Started
### Clone Repository
```
git clone 
cd INC500LIST
```
### Install Dependencies:
> We will compile and install python3.6 and docker.io, since the python apt repo doen't work well with Scrapy
sudo bash 

```sudo bash dependencies.sh```
###Run Scrapper:
```sudo bash start.sh```

###Checkout Result:
Results are placed inside the `output` folder.

## Compatibility

Tested on Ubuntu-16.04 x64.

##Screenshots


## Contact

-  Mailing me for support: ```zerqqr1@iydhp.com```
-  Connect me on wechat: ```ITXSG_HF```

##Acknowledgments
- My Partner - ZePing Bai - Technology Support - Connect him on QQ by QR:
![ZePing Bai]()
- Join our QQ technology forum: ```560956108```
